package invoicing;

public class Product {
    private int prodId;
    private String prodName;
    private double price;
    private String prodDesc;

    public int getProdId() {
        return prodId;
    }

    public void setProdId(int prodId) {
        this.prodId = prodId;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getProdDesc() {
        return prodDesc;
    }

    public void setProdDesc(String prodDesc) {
        this.prodDesc = prodDesc;
    }

    public Product(int prodId, String prodName, double price, String prodDesc) {
        this.prodId = prodId;
        this.prodName = prodName;
        this.price = price;
        this.prodDesc = prodDesc;
    }

    @Override
    public String toString() {
        return  prodId +"\t\t"+ prodName + '\t' + price +'\t' + prodDesc;
    }
}

