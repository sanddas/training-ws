package invoicing;
public class Customer {
 private int custid;
 private String custName;
 private String custEmail;
 private String custAddr;
 private String custPhone;

    public int getCustid() {
        return custid;
    }

    public void setCustid(int custid) {
        this.custid = custid;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public String getCustAddr() {
        return custAddr;
    }

    public void setCustAddr(String custAddr) {
        this.custAddr = custAddr;
    }

    public String getCustPhone() {
        return custPhone;
    }

    public void setCustPhone(String custPhone) {
        this.custPhone = custPhone;
    }

    public Customer(int custid, String custName, String custEmail, String custAddr, String custPhone) {
        this.custid = custid;
        this.custName = custName;
        this.custEmail = custEmail;
        this.custAddr = custAddr;
        this.custPhone = custPhone;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "custid=" + custid +
                ", custName='" + custName + '\'' +
                ", custEmail='" + custEmail + '\'' +
                ", custAddr='" + custAddr + '\'' +
                ", custPhone='" + custPhone + '\'' +
                '}';
    }
}
