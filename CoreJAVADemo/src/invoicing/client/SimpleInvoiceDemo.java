package invoicing.client;

import invoicing.Customer;
import invoicing.Product;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class SimpleInvoiceDemo {
    public static void main(String[] args) {
        /**
         * 1) Create an array of products with some dummy date
         * 2) Create an array of customer with some data
         * 3) Display a Menu to the user
         *   1. see the list of products  - iterate over the array and display all products ina tabular format  id, name price Description
         *   2. to purchase a product
         *      ask the user to enter the email and validate if customer with the email exists in the array.
         *      if no display a message you need to register before continue to purchase.
         *      if yes as the user to enter the products id and quantity. display the total amount customer need topay in the following format:
         *          Date: localdate.now()
         *          Customer Name:
         *          Customer Phone:
         *          Customer email:
         *
         *          Product Name | Price | Quantity | Total
         *
         */

        Customer custList[] = new Customer[3];
        Product  prodList[] = new Product[3];

        custList[0]= new Customer(1001,"Mickey","mickey@disney.com","1 Clubhouse Way","1112223333");
        custList[1]= new Customer(1002,"Minnie","minnie@disney.com","2 Clubhouse Way","2222333344");
        custList[2]= new Customer(1003,"Pluto","pluto@disney.com","3 Clubhouse Way","444455333344");

        prodList[0] = new Product(4001,"Cheese",51.33,"Gouda");
        prodList[1] = new Product(4002,"Bread",4.43,"Ciabatta");
        prodList[2] = new Product(4003,"Cookie",6.56,"Chocolate Chip");

        Scanner sc= new Scanner(System.in);

        while (true){  //main loop

            //Display Menu
            System.out.println("==========Menu===========");
            System.out.println("|1. View List of Products|");
            System.out.println("|2. Purchase a Product   |");
            System.out.println("|3. exit                 |");
            System.out.println("==========================");

            int selection=sc.nextInt(); //accept choice

           if (selection==1){  // display price list
               System.out.println("ID   \t\tName\tPrice\tDescription"); //display product list header
               System.out.println("==   \t\t====\t=====\t==========="); //display product list header

               for(int i=0;i<prodList.length;i++)
                 System.out.println(prodList[i]);
            }
           if(selection==2){ // purchase and price

               System.out.println("Enter email address: ");
               String emailId=sc.next();
               int i;
               // array to hol product purchased details
               int purProduct[]=new int[5];
               double purPrice[]=new double[5];
               int purQuantity[]=new int[5];
               double purTotal[]=new double[5];
               //////////////////////////////////
               for( i=0;i<custList.length;i++){  //find the customer
                   int k=0;
                   if(emailId.equals(custList[i].getCustEmail())){ // if email id matched, customer is found
                       // customer exist
                       double total=0;
                   while(true) {  // take multiple orders

                       System.out.println(("Enter product id"));
                       int product = sc.nextInt();
                       System.out.println("Enter quantity");
                       int quantity = sc.nextInt();
                       int j;
                       for (j = 0; j < prodList.length; j++) {  //scan the product list to find product price
                           if (product == prodList[j].getProdId()) { // if product found
                               //save purchase details in an array for later display
                               purTotal[k] = prodList[j].getPrice() * quantity;
                               purProduct[k]=prodList[j].getProdId();
                               purPrice[k]=prodList[j].getPrice();
                               purQuantity[k]=quantity;
                               /////////
                               k++; // keep count of how many products are being ordered
                               break; //product found and price calculated
                           }

                       }
                       if(j==prodList.length) {
                           System.out.println("**Product not found**");
                       }
                       System.out.println("Want more product? 1(yes)/0(no)");
                       int response=sc.nextInt();
                       if(response==0)
                           break;  // break if customer choose 0
                   } //done with taking order
                    //print invoice
                       DateTimeFormatter df = DateTimeFormatter.ofPattern("HH:mm:ss");
                       LocalDateTime now=LocalDateTime.now();
                       System.out.println("Order Time: "+df.format(now));

                       System.out.println("Customer Name: "+custList[i].getCustName());
                       System.out.println("Customer email: "+custList[i].getCustEmail());
                       System.out.println("Customer phone: "+custList[i].getCustPhone());
                       System.out.println("\nID   \t\tPrice\tQuantity\t\tTotal");
                       System.out.println("==   \t\t=====\t========\t\t=====");

                       for(int x=0;x<k;x++){
                           System.out.println(purProduct[x]+"\t\t"+purPrice[x]+"\t"+purQuantity[x]+"\t\t\t\t"+purTotal[x]);
                       }

                    break; //customer found , order processed, exit for loop
                   }

               }
              if(i==custList.length)//customer not found
                  System.out.println("Sorry, You need to register first:");

           }
           if(selection==3){
               break; //exist program
           }
        // go back to menu selection
        }
    }
}
