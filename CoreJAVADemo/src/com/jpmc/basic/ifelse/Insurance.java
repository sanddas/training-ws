package com.jpmc.basic.ifelse;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.Scanner;


public class Insurance {
    public static void main(String[] args) {
        /**
         * if the user is married =? he/she is insured
         * if the user user is unmarried, male and age >=30, insured
         * if the user user is unmarried, female, age>=25, insured
         * otherwise not insured
         */

    /*    Scanner sc=new Scanner(System.in);
        System.out.println("Enter 'M if married and 'U' if unmarried");
        char status = sc.next().charAt(0);

        if (status=='M')
            System.out.println("person is insured");
        else if(status=='U'){
            System.out.println("Enter 'M' if male and 'F' if female");
            char gender = sc.next().charAt(0);
            System.out.println("Enter Age:");
            int age=sc.nextInt();

            if (gender =='M' && age>=30)
                System.out.println("person is insured");
            else if (gender =='F' && age>=25)
                System.out.println("person is insured");
        }
        else
            System.out.println("Not Insured");

        }*/


        /**
         * Create a program to read total amount on the bill.
         * if bill total is 1000 give 2% discount.
         * if bill total is between 1000 and 5000 (both inclusive) give 5% discount.
         * if bill total is between 5000 and 10000 (both inclusive) give 10% discount.
         * for total greater than 10000 give 20% discount
         *
         * Print the actual amount, discount offered and the discounted amount
         */

/*
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter total bill amount:");
        double totalBillAmt = sc.nextDouble();
       // if (totalBillAmt<1000)
         //   totalBillAmt=totalBillAmt;
        if (totalBillAmt == 1000)
            totalBillAmt = totalBillAmt * .98;
        else if (totalBillAmt > 1000 && totalBillAmt <= 5000)
            totalBillAmt = totalBillAmt * .95;
        else if (totalBillAmt > 5000 && totalBillAmt <= 10000)
            totalBillAmt = totalBillAmt * .9;
        else if(totalBillAmt>10000)
            totalBillAmt = totalBillAmt * .8;

        System.out.println("Final bill after iscount: " + totalBillAmt);
 */

/**
                * Create a program to take annual salary as input,
        * loan amount and period.
        *
        * loan is approved for max amount that is 5 times the salary of the user
        * Term Interest Rate
                        * < 2 Years 5%
        * 2-5 Years 6%
        * 5-8 Years 8%
        * >8 Years 8.5%
   */

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter annual Salary:");
        double annualSalary = sc.nextDouble();
        System.out.println("Enter Loan Amount:");
        double loanAmount = sc.nextDouble();
        double interestRate=0;

        if (loanAmount<=annualSalary*5)
        {
            System.out.println("Enter loan period in years:");
            double loanPeriod = sc.nextDouble();

            if (loanPeriod<2)
            {
                interestRate=2; // annual percentage
            }
            else if (loanPeriod>=2 && loanPeriod<5){
                interestRate=6; // annual percentage
            }
            else if (loanPeriod>=5 && loanPeriod<8){
                interestRate=8; // annual percentage
            }
            else if (loanPeriod>8){
                interestRate=8.5; // annual percentage
            }
            loanPeriod=loanPeriod*12; // convert to monthly interest,not the percentage
            interestRate=interestRate/(12*100); // convert to month
            double emi= (loanAmount*interestRate*Math.pow(1+interestRate,loanPeriod))/(Math.pow(1+interestRate,loanPeriod)-1);

            System.out.println("Loan is approved and interest monthly rate is:"+(interestRate*100) +" and EMI is: "+emi);
        }
        else
            System.out.println("Loan is not approved");


    }
}
