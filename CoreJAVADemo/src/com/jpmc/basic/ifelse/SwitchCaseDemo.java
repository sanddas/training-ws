package com.jpmc.basic.ifelse;

import javax.xml.transform.Result;
import java.util.Scanner;

public class SwitchCaseDemo {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        double result=0;
        System.out.println("Enter Two Numbers:");
        int num1=sc.nextInt();
        int num2=sc.nextInt();
        System.out.println("Enter an operator (+, - , *, /");
        char opChar= sc.next().charAt(0);
        System.out.println(opChar);


        switch (opChar) {
            case '+':
                result = num1 + num2;
                break;
            case '-':
                result = num1 - num2;
                break;
            case '*':
                result = num1 * num2;
                break;
            case '/':
                result = num1 / num2;
                break;
            default:
               System.out.println("Invalid operator");
        }
        System.out.println(result);
    }
}
