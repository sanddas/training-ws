package com.jpmc.basic.ifelse;

import java.util.Scanner;

public class LeapYear {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);

        System.out.println("Enter Year:");

        int year=sc.nextInt();

        if (year%4==0 && year%100!=0){
            System.out.println("Its Leap year");
        }
        else if (year%100==0 && year%400==0){
            System.out.println("Its Leap year");
        }
        else
            System.out.println("Not Leap Year");

    }
}
