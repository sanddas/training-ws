package com.jpmc.basic.assignment;

public class assignment1 {
    static String o="";  // variable for assignment #6
    public static void main(String[] args) {
        /* Assignment #1
         */
     /*   int a = 28, b = 39;
        boolean e = false;
        while(a > 1 && b > 1)
        {
            if(a>b) a = a-b;
            else b = b-a;
        }
        if(a ==1 || b ==1 ) e = true;
        System.out.println(e);*/
      /*Output = True* /
      // End of Assignment #1
       */

        /** Multiple choice questions*/
        // #2 Java language has support for which of the following types of comment - a. block, line and javadoc
        // #3 In Java, the word true is - b. A boolean literal
        // #4 Java uses ___ to represent characters - Unicode
        // #5 Which one is a valid declaration of a Boolean - c. boolean b3 = false;

        /* Assignment #6*/

        z:
        for(int x=2;x<7;x++){

            if(x==3) continue;
            if(x==5)
                break z;
            o=o+x;
        }
        System.out.println(o);
        // output = 24 ---end of assignment #6

        // Assignment#7
        /*while(true?true:false)
        {
            System.out.println("hello");
            break;
        }*/
         // output hello . Answer yes, print statement will be executed --End of assignment#7

        // Assignment #8

     /*   char c = '\u0042';
        switch(c) {
            default:
                System.out.println("Default");
            case 'A':
                System.out.println("A");
            case 'B':
                System.out.println("B");
            case 'C':
                System.out.println("C");
        } */
        // out put will be B,C. unicode character is 'B'. as no break statement is given, code will execute everything after case 'B'. hence B and C will be printed
        // End of assignment #8


    }
}
