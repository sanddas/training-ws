package com.jpmc.basic.assignment;

import sun.plugin.dom.html.ns4.HTMLImageCollection;

import java.util.Scanner;

public class Assignment1Programs {
    public static void main(String[] args) {
        //Assignment #1
        /*
        Take input as 6 digit ticket number. Eg : 123454
        Then remove the last digit in this case 4 remaining is 12345
        This no 12345 if divided by 7 should give the remainder 4 (the last no that we removed).
        Then compare the remainder with the last digit of the 6 digit ticket no.
        Display the result if no is valid or not
        Try for no’s : 147103(should give valid) and 154123 should give invalid
         */

       /* String strTicketNumber="";
        String strTicketNumber5Digit, strTicketLastDigit;
        Scanner sc=new Scanner(System.in);
        strTicketNumber = sc.next();
        int ticketNumber=Integer.parseInt(strTicketNumber);
        strTicketNumber5Digit=strTicketNumber.substring(0,5);
        strTicketLastDigit=strTicketNumber.substring(5);
        int ticketNumber5Digit=Integer.parseInt(strTicketNumber5Digit);
        int ticketLastDigit=Integer.parseInt(strTicketLastDigit);
        String result = (ticketNumber5Digit%7==ticketLastDigit)?"Valid Ticket Number":"Invalid Ticket Number";
        System.out.println(result);*/

        // End of Assignment #1


        // Assignment #2

        /*Create an application to compute final price of sales transactions
        In main method take input for price of an item in RS , Salesperson commission in % and
        customer discount in %.
        Write a program that calculates final price of an item by the formula
        Final_price = (price + sales commission ) – customer discount.
        The customer discount is taken as a percentage of total price after sales person
        commission has been added to the original price.*/

       /* int price, salesCommission, customerDiscount;
        Scanner sc= new Scanner(System.in);

        System.out.println("Enter Price, Sales Commission and Customer Discount");
        price=sc.nextInt();
        salesCommission=sc.nextInt();
        customerDiscount=sc.nextInt();
        int finalPrice=price + (price* salesCommission/100);
        finalPrice=finalPrice - (finalPrice*customerDiscount/100);
        System.out.println("final Price"+finalPrice);*/

        //End of assignment program #2


        // Assignment program #3
        /* The distance between the 2 cities is provided in kms. Write a program to convert and
            find the distance in meters and feets.*/

       /* int distantInKm;
        Scanner sc= new Scanner(System.in);
        System.out.println("Enter distant between two cities in Km");
        distantInKm = sc.nextInt();

        System.out.println("Distant in Meter: "+distantInKm*100+"Distant in Feets: "+distantInKm*3280.84); // 1km = 3280.84 feets*/

        ///End of assignment program #3

        // Assignment program #4
        /*
        Write a program that prints the factors of a number.
            For ex: Input : 15
            Output: 1,3,5
         */

      /*  int numToFactor;
        Scanner sc= new Scanner(System.in);
        System.out.println("Enter a number to factor");
        numToFactor = sc.nextInt();

        System.out.print("Factors of " + numToFactor + " are: ");

        // loop runs from 1 to number entered

        for (int i = 1; i <= numToFactor; ++i) {

            // if number is divided by i
            // i is the factor
            if (numToFactor % i == 0) {
                System.out.print(i + " ");
            }
        }*/
       //End of Assignment program #4

        //Assignment program #5
        /*Two numbers are entered through the keyboard. Write a program to find the value of
        one number raised to the power of another. (Do not use Java built-in method)*/

      /*  int numBase, numPower, result;
        Scanner sc= new Scanner(System.in);
        System.out.println("Enter a base number and it power");
        numBase = sc.nextInt();
        numPower=sc.nextInt();

        // loop runs from 1 to numPower entered
        result = numBase;
        for (int i = 2; i <= numPower; ++i) {
            result=result*numBase;
        }

        System.out.println(numBase+ "To the Power"+numPower+" is Equal to:"+ result);*/

        //End of Assignment program #

        //Assignment program #6
        /*
        Write a do-while loop that asks the user to enter two numbers. The numbers should be
        added and the sum displayed. The loop should ask the user whether he or she wishes to
        perform the operation again. If so, the loop should repeat; otherwise it should
        terminate.
         */
        int num1,num2;
        String strAns;
        boolean ans;
        Scanner sc=new Scanner(System.in);
        do{
            System.out.println("Enter a Number");
            num1=sc.nextInt(); 
            num2=sc.nextInt();

            System.out.println("Sum of "+num1+" and "+num2+" is: "+(num1+num2));
            System.out.println("Do you want to continue (yes/no)?");
            strAns=sc.next();
            ans= (strAns.compareTo("yes")==0)?true:false;
        }while (ans);


    }
}
