package com.jpmc.basic.loops;

import javax.sound.midi.Soundbank;
import java.util.Scanner;

public class NestedLoops {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        /*int i,j,multi;
        for (i=1;i<=10;i++) {
            for (j = 1; j<=10;j++){
                multi=i*j;
                System.out.print(multi + "\t");
            }
            System.out.println("\n");
        }*/

        /**
         *  take input how many customers and each customer as for how many bills and the customer name
         *  print total
         */
       /* int numCustomer, numBills;
        String custName;
        int total;
        System.out.println("Enter number of customer");
        numCustomer=sc.nextInt();
        for (int i = 0; i < numCustomer; i++) {
            System.out.println("Enter Customer Name:");
            custName=sc.next();
            System.out.println("Enter Number of Bills");
            numBills=sc.nextInt();
            total=0;
            for (int j = 0; j <numBills ; j++) {
                System.out.println("Enter Bill Amount");
                int billAmt=sc.nextInt();
                      total=total+      billAmt;
            }
            System.out.println("Name\t\t Total Bill Amt");
            System.out.println(custName+"\t\t"+total);
        }*/

        /**
         * 1) ta 5 nos as input from the user and print only the prime number
         * 2) take 5 nos as input from the user. every number entered should not be a single digit. if it is single digit as the user to enter again till a number is not entered
         * print he sum of all the digits of that number
         */
      /*  for (int i = 1; i <=5; i++) {
            boolean isPrime=true;
            System.out.println("Enter a number:");
            int userInput=sc.nextInt();
            for (int j = 2; j < userInput; j++) {
                if(userInput%j==0) {
                    isPrime=false;
                    break;
                }
             }
            if(isPrime)
            System.out.println("Number"+userInput+"is prime");
            else
                System.out.println("Number"+userInput+"is not prime");

        }*/
        /**
* 2) take 5 nos as input from the user. Every number enetered should not be a single digit. If it
* is single digit ask the user to enter again till a proper number is not entered
* Print the sum of all the digits of that number
*
* Ex: Input 21 123 345 678 29
           */
        for (int i = 1; i <=5 ; i++) {
            int sumDigit=0;
            int userInput=sc.nextInt();
            if(userInput/10<1)
                continue;
            while(true){
                int digit=userInput%10;
                sumDigit=sumDigit+digit;
                userInput=userInput/10;
                if (userInput==0)
                    break;
            }
            System.out.println(sumDigit);

        }


    }
}
