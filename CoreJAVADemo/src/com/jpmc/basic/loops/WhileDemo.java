package com.jpmc.basic.loops;

import com.sun.corba.se.impl.naming.cosnaming.NamingUtils;

import java.util.Scanner;

public class WhileDemo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);


     /*  int count=1;
        while(count<=5) {

            System.out.println("Enter a Number");
            int input=sc.nextInt();
            System.out.println("Square of "+input+ "is "+Math.pow(input,2));
            count++;
        }*/
       /* int count = 1;
        int amtInvoice = 0;
        int sumInvoice = 0;
        while (true) {
            System.out.println("Enter amount of invoice" + count);
            amtInvoice = sc.nextInt();
            if (amtInvoice == -1)
                break;
            if (amtInvoice > 1000) {
                sumInvoice = sumInvoice + amtInvoice;
                count++;
            }
        }
        System.out.println("Number of Invoices "+--count +" and total invoice amount is "+sumInvoice);*/
       /* int i=1;
        do {
            System.out.println(i);
            i++;
        }while(i<=5);*/

        /**
         * suppose you are edsigining an application for ticket counter
         * Implement the following using do-while
         * 1. as the clerk the choice
         * age price
         * below 5 free
         * >5 to 15 100
         * >15 150
         * 2. purchase tickets
         * as customer name, no of people, age of each person and calculate total amount to be paid by the custoemr
         * 3. exit
         * the program continues to take input until clerk wishes to exit
         */
        int clerkChoice = 0;
        Boolean continueProgram=true;

        do{
            System.out.println("Enter choice: \n1. display ticket details \n2. purchase tickets\n3. exit");
            clerkChoice=sc.nextInt();

            switch (clerkChoice){
                case 1:
                    System.out.println("\tAge\tprice");
                    System.out.println("\tbelow 5\t free");
                    System.out.println("\t>5 to 15\t100");
                    System.out.println("\t>15\t1500");
                    break;
                case 2:
                    System.out.println("Enter Customer Name");
                    String strCustName=sc.nextLine();
                     strCustName=sc.nextLine();

                    System.out.println("Enter number of tickets:");
                    int numTickets=sc.nextInt();
                    int age=0;
                    int totalTicketPrice=0;
                    for(int i=1;i<=numTickets;i++){
                        System.out.println("Enter Age of person #"+i);
                        age=sc.nextInt();

                        if (age<5)
                            totalTicketPrice=totalTicketPrice; //do nothing
                        else if(age >5 && age <=15)
                            totalTicketPrice=totalTicketPrice+100;
                        else if (age  >15)
                            totalTicketPrice=totalTicketPrice+150;
                       }

                    System.out.println("Total Ticket price is: "+totalTicketPrice +" For Customer "+strCustName);
                    break;
                case 3:
                    continueProgram=false;
                    break;
            }
        }while(continueProgram);
    }
}
