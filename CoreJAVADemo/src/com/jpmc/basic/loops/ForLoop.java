package com.jpmc.basic.loops;

import java.util.Scanner;

public class ForLoop {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);


     /*   int product=1;
        int numValue;
        for(int count=1;count<=5;count++){
            System.out.println("Enter number #" + count);
            numValue = sc.nextInt();
            product=product*numValue;
        }
        System.out.println(product);*/

        /**
         * find max of five numbers
         */
        int max=0;
        int num=0;
        num=sc.nextInt();
        max=num;
        for (int i=1;i<=4;i++){
            num=sc.nextInt();
            if (max<num)
                max=num;
        }
        System.out.println(max);

    }
}
