package com.jpmc.basic.basicclass;

public class Carton {
    int noOfEggs;
    int eggsPerCarton;
    void set(int noOfEggs,int eggsPerCarton){
        this.noOfEggs=noOfEggs;
        this.eggsPerCarton=eggsPerCarton;
    }

    void calcNumberOfCarton()
    {

        System.out.println("Number of carton: "+this.noOfEggs/this.eggsPerCarton);
        System.out.println("Number of remaining eggs: "+this.noOfEggs%this.eggsPerCarton);
    }
}
