package com.jpmc.basic.basicclass;

import java.util.Scanner;

public class TicketCounter {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        Ticket tc=new Ticket();
        System.out.println("Enter ticket Number");
        int ticketNumber=sc.nextInt();
        System.out.println(("Enter Digit"));
        int digit=sc.nextInt();

        tc.setTicketNumber(ticketNumber);
        int price=tc.checkReward(digit);

        if (price>0)
            System.out.println("Reward: "+price);
        else
            System.out.println("Sorry, Next Time");


    }
}
