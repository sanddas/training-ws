package com.jpmc.basic.basicclass;

public class Ticket {
    int ticketNumber;
    int price;

    void setTicketNumber(int ticketNumber){
        this.ticketNumber=ticketNumber;
    }

    int checkReward(int digit){
        int lastDigit=ticketNumber%10;
        if (digit==lastDigit){
          price=ticketNumber*digit+10;
        }
        else
            price=0;
        return price;

    }
}
