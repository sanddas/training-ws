package com.jpmc.arrays;

public class ArrayLoopDemo {

    public static void main(String[] args) {
        // shorthand syntax to initialize the array
        // declares, creates and initializes the array
        int nos[] = {10,12,34,200,7,9};
        System.out.println(nos.length);// 6
        // 0 to length-1 : 0 1 2 3 4 5
        // nos.length = 6
        System.out.println("Name\tSquare\tCube");
        for(int i = 0; i < nos.length; i++)
        {
            int n = nos[i]; // nos[0] = 10
            System.out.println(n+"\t\t"+n*n+"\t\t"+n*n*n);

           // System.out.println(nos[i]+"\t\t"+nos[i]*nos[i]+"\t\t"+nos[i]*nos[i]*nos[i]);
        }
        /**
         * print the output as follows:
         *
         * No Square Cube
         * 10  100   1000
         * 12  144
         * .
         */



        // nos.length - 1 = 6 -1 = 5
//        for(int i = 0;i <= nos.length - 1; i++)
//        {
//            System.out.println(nos[i]);
//        }

    }
}
