package com.jpmc.arrays;

public class ArrayDemo {

    public static void main(String[] args) {
        /**
         * 1) Arrays are special variables that can store more than 1 value BUT of SAME DATA TYPE
         * 2) The size needs to be declared which means the no of items it can store
         * want to store 5 numbers of type int => 5 * 4 = 20 bytes of memory
         * 3) use [] as a syntax to declare an array variable
         * 4) supports indexing to access a value inside an array
         */
        // declare a variable
        int x;
        // initialize
        x = 10;

        // declare an array variable
        int [] arr ;

        // creation or size of an array
        // new is a keyword that allocates memory for your array
        arr = new int[5];
        //arr[] = new int [5];
        // declaration adn creation in single statement
        String names[] = new String[3];
        System.out.println(arr.length); // no of elements that an array can hold
        System.out.println(arr);// [I@61bbe9ba
        System.out.println(x);
        // initialize values in the array
        System.out.println("arr[3] "+arr[3]);
        System.out.println("names[0] "+names[0]);
        arr[0] = 5;
        arr[1] = 9;
        arr[2] = 10;
        arr[3] = 6;
        arr[4] = 8;
        // display values from the array
        System.out.println(arr[3]);
        char ch[] = new char[2];
        System.out.println("ch[0] "+ (ch[0]=='\u0000'));
    }
}
