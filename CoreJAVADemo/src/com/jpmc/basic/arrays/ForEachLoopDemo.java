package com.jpmc.basic.arrays;

public class ForEachLoopDemo {
    public static void main(String[] args) {
        int x[]={10,20,30,40,50};

        //can only be forward movement
        //cannnot modify the array on which it is iterating
        for(int value:x)
            System.out.println(value);

    }
}
