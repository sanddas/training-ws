package com.jpmc.arrays;

import java.util.Scanner;

/**
 * WAP to ask the user for total no of bills
 * create an array to store amount for no of bills entered
 * ask the user to enter the bill amount and store in the array
 * the amount can be decimal values
 * Display the total of all the bills
 */
public class InvoiceAmountArray {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the no of bills");
        // asking the user to enter the total no of bills for which the total amount needs to be calcualted
        int bill = sc.nextInt(); // bill = 3

        // creating an array to store amounts for the no of bills
        double amount[] = new double[bill];

        // used to iterate for storing amount on every bill in the amount[]
        for (int count = 0; count < amount.length; count++) {
            System.out.println("enter amount for bill no "+(count+1));
            amount[count] = sc.nextDouble();
        }
        double total = 0;
        for (int i = 0; i < amount.length; i++) {
            total = total + amount[i];
        }
        System.out.println("Bill No \t Amount");
        for (int i = 0; i < amount.length; i++) {
            System.out.println("Bill "+(i+1)+"\t\t"+amount[i]);
        }
        System.out.println("---------------------------------------------------");
        System.out.println("\t\t\t\t Total  = "+total);
    }
}
