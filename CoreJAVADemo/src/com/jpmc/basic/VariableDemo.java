package com.jpmc.basic;

public class VariableDemo {
    public static void main(String[] args) {

        /* they can start with as alphabet, _ or $
        they cannot start with a number, space or any other special char
        they can contain numbers or _ or $ or alphabets but no other symnbols
        cant be keywords
         */

        int x=20;
        System.out.println(x + " is stored in x");
    }
}
