package com.jpmc.basic.jump;

import java.util.Scanner;

public class BreakDemo {
    public static void main(String[] args) {

        /**
         * Password verification with max attempt of 5
         */
        Scanner sc=new Scanner(System.in);

        String correctPassword="Password1234";

        int numAttempt=0;
        while(true)
        {
            System.out.println("Enter password");
            String password = sc.next();
            numAttempt++;
            if(password.compareTo(correctPassword)==0) {
                System.out.println("Password Matched. Welcome !!");
                break;
            }
            else
                System.out.println("Incorrect password");
            if(numAttempt>5) {
                System.out.println("Too many number of tries. Bye Bye");
                break;
            }

        }
    }
}
