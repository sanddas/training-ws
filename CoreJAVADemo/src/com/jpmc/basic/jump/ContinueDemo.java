package com.jpmc.basic.jump;

import java.util.Scanner;

public class ContinueDemo {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);

        for (int i=1;i<=10;i++){
            System.out.println("Enter a Number:");
            int userNum=sc.nextInt();
            if(userNum%5==0){
                System.out.println("Number skipped, multiple of 5");
                continue;
            }
            System.out.println(Math.pow(userNum,2));
        }

    }
}
