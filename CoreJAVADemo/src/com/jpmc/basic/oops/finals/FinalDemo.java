package com.jpmc.basic.oops.finals;

public class FinalDemo {
    public static void main(String[] args) {
        String url = "http://www.demo.com/folder/sunset.png";

        System.out.println(url.substring(url.lastIndexOf('/')+1));
    }

}
