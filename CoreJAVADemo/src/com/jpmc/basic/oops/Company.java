package com.jpmc.basic.oops;

import java.util.StringTokenizer;

public class Company {
    private int cid;
    private String cname;
    private String location;

    public Company() {
        //default
        cid = 0;
        cname = "Not Available";
        location = "Default";
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Company(int cid, String cname) {

    this.cid=cid;
    this.cname=cname;
    this.location="Default";
    }

    public Company(int cid, String cname, String location){
        this.cid=cid;
        this.cname=cname;
        this.location=location;
    }

    @Override
    public String toString() {
        return "Company{" +
                "cid=" + cid +
                ", cname='" + cname + '\'' +
                ", location='" + location + '\'' +
                '}';
    }

  //  public void show(){
  //      System.out.println("CID: "+cid+" Cname: "+cname+" Location: "+location);
  //  }

}
