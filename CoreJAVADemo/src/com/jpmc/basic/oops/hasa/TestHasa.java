package com.jpmc.basic.oops.hasa;

import java.time.LocalDate;

public class TestHasa {
    public static void main(String[] args) {
      //  Address add1=new Address("Newark","DE","19702");
        Author a1 = new Author(1, "Dr. Seus", "kid");
        Author a2=new Author(2,"Herge","fun",new Address("Newark","DE","19702"));
    //    Book book = new Book(1, "Danger", 100.01, a1, LocalDate.now());

        System.out.println(a1);
        System.out.println(a2);
    }
}
