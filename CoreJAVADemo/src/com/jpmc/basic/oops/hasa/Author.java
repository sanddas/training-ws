package com.jpmc.basic.oops.hasa;

public class Author {
    private int aid;
    private String aname;
    private String genre;
    private Address address;

    public Author(int aid, String aname, String genre) {
        this.aid = aid;
        this.aname = aname;
        this.genre = genre;
    }

    public Author() {
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Author(int aid, String aname, String genre, Address address) {
        this.aid = aid;
        this.aname = aname;
        this.genre = genre;
        this.address = address;
    }

    @Override
    public String toString() {
        return "Author{" +
                "aid=" + aid +
                ", aname='" + aname + '\'' +
                ", genre='" + genre + '\'' +
                ", address=" + address +
                '}';
    }

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public String getAname() {
        return aname;
    }

    public void setAname(String aname) {
        this.aname = aname;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

}
