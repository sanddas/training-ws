package com.jpmc.basic.oops.hasa;

import java.time.LocalDate;

public class Book {

    private int bookid;
    private String title;
    private  double price;
    private Author author;
    private LocalDate pubDate;

    public Book() {
    }

    public Book(int bookid, String title, double price, Author author, LocalDate pubDate) {
        this.bookid = bookid;
        this.title = title;
        this.price = price;
        this.author = author;
        this.pubDate = pubDate;
    }

    public int getBookid() {
        return bookid;
    }

    public void setBookid(int bookid) {
        this.bookid = bookid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public LocalDate getPubDate() {
        return pubDate;
    }

    public void setPubDate(LocalDate pubDate) {
        this.pubDate = pubDate;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookid=" + bookid +
                ", title='" + title + '\'' +
                ", price=" + price +
                ", author=" + author +
                ", pubDate=" + pubDate +
                '}';
    }
}
