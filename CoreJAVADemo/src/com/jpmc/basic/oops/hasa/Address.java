package com.jpmc.basic.oops.hasa;

public class Address {
    private String city;
    private String State;
    private String zipcode;

    public Address() {
    }

    public Address(String city, String state, String zipcode) {
        this.city = city;
        this.State = state;
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    @Override
    public String toString() {
        return "Address{" +
                "city='" + city + '\'' +
                ", State='" + State + '\'' +
                ", zipcode='" + zipcode + '\'' +
                '}';
    }
}
