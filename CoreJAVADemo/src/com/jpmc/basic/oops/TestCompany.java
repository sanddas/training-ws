package com.jpmc.basic.oops;

public class TestCompany {
    public static void main(String[] args) {
        Company c1=new Company();
        System.out.println(c1);
        Company c2 = new Company(7,"Sandip");
        System.out.println(c2);

        Company c3 = new Company(101,"Sparkey","Colorado");
        System.out.println(c3);

    }
}
