package com.jpmc.basic.oops.Inheritance;

public abstract class Item {
    private int itemID;
    private String description;
    private double price;

  //  public Item() {
  //     System.out.println("Item default const");

    public abstract void calculation();
    @Override
    public String toString() {
        return "Item{" +
                "itemID=" + itemID +
                ", description='" + description + '\'' +
                ", price=" + price +
                '}';
    }

    public Item(int itemID, String description, double price) {
        this.itemID = itemID;
        this.description = description;
        this.price = price;
    }

    public int getItemID() {
        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
