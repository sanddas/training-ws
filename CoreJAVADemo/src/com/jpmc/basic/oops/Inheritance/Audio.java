package com.jpmc.basic.oops.Inheritance;

public class Audio extends Item {
    private String artist;



    public Audio(String artist) {
        super(1,"abcd",1.09);

        this.artist = artist;
    }

    @Override
    public String toString() {
        return "Audio{" +
                "artist='" + artist + '\'' +
                '}';
    }
    public  void calculation(){
        System.out.println("Calcutate audio price");
    }
    public Audio() {
        super(1,"abcd",1.09);

        System.out.println("Audion item const");
    }


    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }


}
