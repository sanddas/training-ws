package com.jpmc.basic.oops.Inheritance;


public class Book extends Item{
    private String title;
    private String author;

    public Book(String title, String author) {
        super(1,"abcd",1.09);

        this.title = title;
        this.author = author;
    }

    public Book() {
        super(1,"abcd",1.09);
        System.out.println("Book default const");
    }
    public  void calculation(){
        System.out.println("Calcutate book price");
    }
    @Override
    public String toString() {
        return  super.toString() +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
