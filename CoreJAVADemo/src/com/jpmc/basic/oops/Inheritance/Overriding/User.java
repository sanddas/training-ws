package com.jpmc.basic.oops.Inheritance.Overriding;
import java.lang.Math;
public class User {
    private int id;
    private String name;
    private String email;

    public User() {
    }

    public User( String name, String email) {
        this.id = 1;
        this.name = name;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
    public int generateID(){
        int rand=1;
  //      double rand= Math.random()*10;
        return (int)rand ;
    }
}
