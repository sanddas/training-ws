package com.jpmc.basic.oops.Inheritance.Overriding;

public class Employee extends User {
    private int salary;

    public Employee() {
    }

    public Employee( String name, String email, int salary) {
        super(name,
                email);
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return super.toString() +
                "salary=" + salary +
                '}';
    }
    public int generateID(){
        int rand=2;
        System.out.println("Employee User ID is generated");
        return (int)rand ;
    }
}
