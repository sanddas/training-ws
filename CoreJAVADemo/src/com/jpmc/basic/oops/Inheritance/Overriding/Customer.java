package com.jpmc.basic.oops.Inheritance.Overriding;

public class Customer extends User {
    private String birthday;
    private String customerTier;

    public Customer() {
    }

    public Customer( String name, String email,String birthday, String customerTier) {
        super(name,email);
        this.birthday = birthday;
        this.customerTier = customerTier;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCustomerTier() {
        return customerTier;
    }

    public void setCustomerTier(String customerTier) {
        this.customerTier = customerTier;
    }

    @Override
    public String toString() {
        return super.toString() +
                "birthday='" + birthday + '\'' +
                ", customerTier='" + customerTier + '\'' +
                '}';
    }
    public int generateID(){
        int rand=3;
        System.out.println("Customer User ID is generated");
        return (int)rand ;
    }
}
