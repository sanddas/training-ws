package com.jpmc.basic.oops.Inheritance.Overriding;

public class Test {
    /**
     * Create a class User with foll data members
     * id, name, email
     * constructor/ getters and setters/ tostring
     * generateId() => sout User id will be generated
     *
     * Create a class employee that extends User with foll member
     * salary
     * constructor/ getters and setters/ tostring
     * override generateId() => sout employee id will be generated
     *
     * Create a class customer that extends User with foll member
     * birthdate
     * constructor/ getters and setters/ tostring
     * generateId() => sout customer id will be generated
     *
     * Create objects for employee and customer and print the repsective details
     */
    public static void displayID(User user){

        Employee emp=null;
        Customer customer =null;
        user.generateID();
        if (user instanceof  Employee){
            emp=(Employee)user;
            System.out.println(emp);
        }
        if (user instanceof  Customer){
            customer=(Customer)user;
            System.out.println(customer);
        }

    }

    public static void main(String[] args) {

        User user1=new Employee("sandip","a@b.c",1000);
        User user2=new Customer("sandip","a@b.c","1/1/2001","Premium");
        displayID(user1);
        displayID(user2);
    }



}
