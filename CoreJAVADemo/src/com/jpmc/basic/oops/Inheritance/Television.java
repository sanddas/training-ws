package com.jpmc.basic.oops.Inheritance;

public class Television extends Item{
    private String type;

    public Television(String type) {
        super(1,"abcd",1.09);

        this.type = type;
    }

    public Television () {
        super(1,"abcd",1.09);

        System.out.println("Television default const");
    }
    public  void calculation(){
        System.out.println("Calcutate television price");
    }
    @Override
    public String toString() {
        return "Television{" +
                "type='" + type + '\'' +
                '}';
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
