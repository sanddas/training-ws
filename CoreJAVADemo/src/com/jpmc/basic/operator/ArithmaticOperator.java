package com.jpmc.basic.operator;

import java.util.Scanner;

public class ArithmaticOperator {
    public static void main(String[] args) {
        int a =12;
        int b=7;
        System.out.println(a+b);
        System.out.println(a-b);
        System.out.println(a*b);
        System.out.println(a/b);
        System.out.println(a%b);

        int quantity, price, total;

        Scanner sc = new Scanner(System.in);
       /* System.out.println("Enter value for price");
        price=sc.nextInt();
        System.out.println("Enter value for quantity");
        quantity=sc.nextInt();
        total=price*quantity;

        */

      //  System.out.println("Price "+ price+"  quantity"+quantity+"   total"+total);
      //  System.out.printf("Price: %d, Quantity: %d, Total: %d",price,quantity,total);

        /* suppose we have notes of following denominations
        500, 100, 50,20,10,5,1
        take the amount as input from the user as calculate how many notes of each denomination will be required
         */

        System.out.println("Enter cash amount:");
        int amount=sc.nextInt();

        int note500,note100,note50, note20,note10, note5,note1;

        note500=amount/500;
        amount=amount%500;

        note100=amount/100;
        amount=amount%100;

        note50=amount/50;
        amount=amount%50;

        note20=amount/20;
        amount=amount%20;

        note10=amount/10;
        amount=amount%10;

        note5=amount/5;
        amount=amount%5;

        note1=amount;

        System.out.printf("500note count: %d, 100Note count: %d, 50note count: %d, 20Note count: %d, 10 note count: %d, 5Note count: %d 1Note count: %d",note500,note100,note50,note20,note10,note5,note1);



    }
}
