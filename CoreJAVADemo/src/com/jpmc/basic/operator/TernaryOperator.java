package com.jpmc.basic.operator;

import java.util.Scanner;

public class TernaryOperator {
    public static void main(String[] args) {
    /* conditional programming

     pub =>21+
     movie => A, age 18 and over


     */

        /*Scanner sc=new Scanner(System.in);
        System.out.println("Enter Age:");
        int age=sc.nextInt();
        String result=(age>=18)?"Yes go ahead and enjoy the movie": "Sorry, you are underage and not allowed";
        System.out.println(result);*/

        /**
         * Take input values of 3 angles and print if a triangle can be formed
         */

        Scanner sc=new Scanner(System.in);

        System.out.println("Enter three angle:");
        int angle1=sc.nextInt();
        int angle2=sc.nextInt();
        int angle3=sc.nextInt();


        String result=(angle1+angle2+angle3==180)?"Yes you can create triangle": "Sorry, not a triangle it is not 180 degree total";

        System.out.println(result);

    }
}
