package com.jpmc.basic.operator;

public class RelationalOperator {
    public static void main(String[] args) {
        int p1=20, p2=20;
        System.out.println(p1>p2);
        System.out.println((p1<p2));
        System.out.println(p1>=p2);
        System.out.println(p1<=p2);
        System.out.println(p1!=p2);
        System.out.println(p1==p2);
    }
}
